#!/usr/bin/env sh
# Compute the mean image lmdb
# N.B. this is available in data/ilsvrc12


TRAIN_LMDB=data/cifar10/10k_4C/10k_4C_train_lmdb
OUTPUT_FOLDER=data/cifar10/10k_4C/
COMPUTE_IMAGEMEAN=build/tools/compute_image_mean.bin

echo "Creating LMDB mean"

$COMPUTE_IMAGEMEAN \
$TRAIN_LMDB \
$OUTPUT_FOLDER/mean.binaryproto

echo "Done."

#!/usr/bin/env sh

# Dieses Script laesst die Trainings-Funktion von Caffe ueber das uebergebene Netzwerk laufen.
# Die "$?"-Werte stehen fuer die Positionen der uebergebenen Parameter.
# $1 path/to/caffe			- Der Pfad zur Caffe Datei
# $2 <netzwerk_train_test.prototxt>	- Der zweite Parameter des Scripts ist der Pfad zur Netzwerk-Datei in der die Topologie abgespeichert wird
# $3 <netzwerk_log.log>			- Der dritte Parameter des Scripts ist der Pfrad des Ordners an dem die log-file des Trainings gespeichert wird.

# Ueberpruefe die Parameter
if [ ! -f $1 ];
then
	echo
	echo "Der Pfad zur Caffe-Datei stimmt nicht " $1
	echo
	exit
fi

if [ ! -f $2 ];
then
	echo
	echo "Der Pfad zur Solver-Datei stimmt nicht " $2
	echo
	exit
fi

if [ ! -d $3 ];
then
	echo
	echo "Der Pfad zur Log-Datei stimmt nicht " $3
	echo
	exit
fi

# Aufruf des Programms

echo
echo "Starte Training."
echo
$1 train --solver=$2 -logtostderr=1 2>&1 | tee $3/net.trainlog
echo
echo "Training beendet."
echo

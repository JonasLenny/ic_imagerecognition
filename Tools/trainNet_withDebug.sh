#!/usr/bin/env sh
#################################################################################
############## Dieses Script nur vom Hauptordner Caffe starten!!! ###############
###### Es arbeitet mit relativen Pfaden und diese stimmen sonst nicht mehr.######
#################################################################################

###### Allgemeine Parameter ######
# Caffe-Pfad client/server
  CAFFE_DIR=./build/tools/caffe
# CAFFE_DIR=/usr/local/caffe-master/build/tools/caffe

# Java-Pfad client/server
  JAVA_PATH=""

# TOOLS_PATH
  TOOLS_PATH=customtools

# Netzwerk-Pfad = examples/<NETWORK_DIR-folder>
NETWORK_DIR=$1							#<--- Netzwerk-Ordner falls noetig aendern

# Netzwerk-OUTPUT_DIR = <NETWORK_DIR>/<OUTPUT_DIR-folder>
OUTPUT_DIR=$NETWORK_DIR/output

# Aktueller Lauf = run_X
ACT_RUN=1

RUN_DIR="run_"$ACT_RUN
RUN_PATH=$OUTPUT_DIR/$RUN_DIR

#Run-Ordner
SNAPSHOT_DIR=snapshots
LOG_DIR=logs
IMAGE_DIR=images
NETWORK_FILES=network

###### File Werte ######
TRAIN_LOGFILE=net.trainlog
FILTERED_LOGFILE=net_filtered.txt

###### Netzwerk Parameter ######

# Netzwerk-Solver = solver.prototxt
SOLVER=solver.prototxt						#<--- Solver-Datei-Name falls noetig aendern

# Netzwerk-Datei = train_test.prototxt
NETWORK=train_test.prototxt					#<--- Netzwerk-Datei-Name falls noetig aendern

# Netzwerk-Deploy = deploy.prototxt
DEPLOY=deploy.prototxt						#<--- Deploy-Datei-Name falls noetig aendern

# Anzahl der Benchmark-Iterationen
BENCHMARK_ITERATIONS=10						#<--- Benchmark-Iterationen falls noetig aendern


echo
echo
echo
echo "##### trainNet Script #####"
echo

#################################################################
############### Erzeugen der Ordner-Strukturen ##################
#################################################################

echo "## Ueberpruefe Ordnerstrukturen ##"

# Ueberpruefe ob der angegebene Netzwerk-Ordner stimmt.
if [ ! -d $NETWORK_DIR ]; 
then
	echo
	echo "#### Error ####"
	echo "Netzwerk-Pfad existiert nicht:" $NETWORK_DIR
	echo "###############"
	echo
	exit
else
	echo "Netzwerk-Pfad: \t [existiert] -" $NETWORK_DIR
fi

# Ueberpruefe ob der OUTPUT_DIR-Ordner bereits existiert
if [ ! -d $OUTPUT_DIR ];
then
	echo "Output-Pfad: [existiert nicht] - wird angelegt"
	mkdir $OUTPUT_DIR
	if [ -d $OUTPUT_DIR ];
	then
		echo "Output-Pfad: [existiert] -" $OUTPUT_DIR
	else
		echo
		echo "#### Error ####"
		echo "Output-Pfad konnte nicht angelegt werden."
		echo "###############"
		echo
		exit
	fi
else
	echo "Output-Pfad: \t [existiert] -" $OUTPUT_DIR
fi
	
# Ueberpruefe ob der gewuenschte run-Ordner existiert

if [ ! -d $RUN_PATH ];
then
	echo $RUN_DIR"-Pfad:\t[existiert nicht] - wird angelegt"
	mkdir $RUN_PATH
	if [ -d $RUN_PATH ];
	then
		echo $RUN_DIR"-Pfad: \t [existiert] -" $RUN_PATH
	else
		echo
		echo "#### Error ####"
		echo $RUN_DIR"-Pfad konnte nicht angelegt werden."
		echo "###############"
		echo
		exit
	fi
else
	isCorrect=false
	while ! $isCorrect; 
	do
		echo $RUN_DIR"-Pfad: \t [existiert bereits] - andere Zahl angeben:"
		read number

		newDIR="run_"$number
		RUN_PATH=$OUTPUT_DIR/$newDIR
		if [ ! -d $RUN_PATH ];
		then
			mkdir $RUN_PATH
			isCorrect=true
		fi
	done
	echo $RUN_PATH"-Pfad: \t [existiert] -" $RUN_PATH
fi

# Erzeuge die benoetigten Ordner
echo
echo "## Erzeuge benoetigte Ordner ##"

mkdir $RUN_PATH/$NETWORK_FILES
echo "Network-Ordner:  [angelegt] -" $RUN_PATH/$NETWORK_FILES
mkdir $RUN_PATH/$SNAPSHOT_DIR
echo "Snapshot-Ordner: [angelegt] -" $RUN_PATH/$SNAPSHOT_DIR
mkdir $RUN_PATH/$LOG_DIR
echo "Log-Ordner: \t [angelegt] -" $RUN_PATH/$LOG_DIR
mkdir $RUN_PATH/$IMAGE_DIR
echo "Images-Ordner: \t [angelegt] -" $RUN_PATH/$IMAGE_DIR


####### Debug ######
if [ $2 ]; then
continue=false

	while ! $continue;
	do
		echo "weiter? [y]"
		read input
		if [ $input = "y" ]; then
			continue=true
		fi		
	done
fi
####################

#################################################################
#################### Aufruf der Programme #######################
#################################################################

echo 
############ Netzwerk-Dateien kopieren ##########
cp -b $NETWORK_DIR/$SOLVER $RUN_PATH/$NETWORK_FILES
echo $SOLVER "-backup gespeichert"

cp -b $NETWORK_DIR/$NETWORK $RUN_PATH/$NETWORK_FILES
echo $NETWORK "-backup gespeichert"

cp -b $NETWORK_DIR/$DEPLOY $RUN_PATH/$NETWORK_FILES
echo $DEPLOY "-backup gespeichert"

#################################################

echo
########## Benchmark-Test laufen lassen #########
./$TOOLS_PATH/benchmark_net.sh $CAFFE_DIR $NETWORK_DIR/$NETWORK $RUN_PATH/$LOG_DIR $BENCHMARK_ITERATIONS
#################################################


############## Starte das Training ##############
## Setze bestimmte Zeilen in der Solver-Datei auf die aktuellen Einstellungen
# Setze den Snapshot-Prefix auf den aktuellen Run-Path
replace_point="snapshot_prefix"
snapshot_prefix="snapshot_prefix: "\"$RUN_PATH/$SNAPSHOT_DIR/\"

sed -i "s|$replace_point.*|$snapshot_prefix|g" $NETWORK_DIR/$SOLVER

./$TOOLS_PATH/train_net.sh $CAFFE_DIR $NETWORK_DIR/$SOLVER $RUN_PATH/$LOG_DIR
#################################################

###### Filtere die Werte aus der Log-File #######
echo
echo "## Filtere benoetigte Werte aus dem Training ##"

log_path=$RUN_PATH/$LOG_DIR
logfile_path=$log_path/$TRAIN_LOGFILE

#java -jar ./$TOOLS_PATH/logfile_visualization_0.2.jar visualize $filtered_logfile_path $image_path
#/home/hezel/jdk1.8.0_20/bin/java -jar ./tools/customTools/logfile_extraction.jar extract $logfile_path $log_path
#################################################

############## Zeichne den Graphen ##############
#echo
#echo "## Zeichne Graphen aus den Trainingswerten ##"

image_path=$RUN_PATH/$IMAGE_DIR
filtered_logfile_path=$log_path/$FILTERED_LOGFILE

#java -jar ./$TOOLS_PATH/logfile_visualization_0.2.jar visualize $filtered_logfile_path $image_path
#/home/hezel/jdk1.8.0_20/bin/java -jar ./tools/customTools/logfile_visualization_0.2.jar visualize $filtered_logfile_path $image_path
#################################################


####### Erzeuge die Bilder des Netzwerks ########
#echo
#echo "## Erzeuge die Bilder aus dem Netzwerk ##"
#deploy_path=/$NETWORK_DIR/$DEPLOY
#cmodel_path=/$RUN_PATH/$SNAPSHOT_DIR/_iter_10000.caffemodel
#imgfile_path=/data/bottles/test/Cola/2_0010.jpg
#imgDir_path=/$RUN_PATH/$IMAGE_DIR
#
#python ./tools/customTools/render_all/render_all.py $deploy_path $cmodel_path $imgfile_path $imgDir_path
#################################################

echo
echo "##### trainNet Script Ende #####"
echo

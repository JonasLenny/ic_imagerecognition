#!/usr/bin/env sh
###
# Starte dieses script aus dem Startverzeichnis von caffe!
# Die relativen Pfade stimmen sonst nicht mehr.
###

# $1 dataSet
#/shared/studenten/nn_master_project/jonas/data/bottles-12/train
#/shared/studenten/nn_master_project/jonas/data/bottles-12/test
# $2 label-list
#/shared/studenten/nn_master_project/jonas/data/bottles-12/train_labels.txt
#/shared/studenten/nn_master_project/jonas/data/bottles-12/test_labels.txt
# $3 destination
#/shared/studenten/nn_master_project/jonas/data/bottles-12/bottles-12_train_lmdb
#/shared/studenten/nn_master_project/jonas/data/bottles-12/bottles-12_test_lmdb

echo "Creating lmdb..."

RESIZE_HEIGHT=32
RESIZE_WIDTH=32
TOOL_PATH=build/tools/convert_imageset

GLOG_logtostderr=1 $TOOL_PATH \
    --resize_height=$RESIZE_HEIGHT \
    --resize_width=$RESIZE_WIDTH \
	--shuffle \
	$1 \
	$2 \
	$3

echo "Done."

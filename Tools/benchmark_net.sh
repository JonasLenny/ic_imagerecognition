#!/usr/bin/env sh

# Dieses Script laesst die Benchmark-Funktion von Caffe ueber das uebergebene Netzwerk laufen.
# Die "$?"-Werte stehen fuer die Positionen der uebergebenen Parameter.
# $1 path/to/caffe			- Pfad an dem Caffe liegt
# $2 <netzwerk_train_test.prototxt>	- Der erste Parameter des Scripts ist der Pfad zur Netzwerk-Datei in der die Topologie abgespeichert wird
# $3 <log-dir>				- Der Ort an dem die Log-file abgespeichert werden soll.
# $4 <iterations>			- Die Anzahl an Iterationen in denen die Zeit gemessen werden soll


# Ueberpruefe die Parameter
if [ ! -f $1 ];
then
	echo
	echo "Der Pfad zur Caffe-Datei stimmt nicht " $1
	echo
	exit
fi

if [ ! -f $2 ];
then
	echo
	echo "Der Pfad zur Netzwerkdatei stimmt nicht " $2
	echo
	exit
fi

if [ ! -d $3 ];
then
	echo
	echo "Der Pfad in dem die log-file geschrieben werden soll existiert nicht " $3
	echo
	exit
fi 

if [ $4 -lt 0 ];
then
	echo
	echo "Verwende eine positive Zahl" $4
	echo
	exit
fi


# Beginne den Benchmark-Test
echo "Starte Benchmark"
echo

#$1 time -model $2 -iterations=$4 --gpu=0 -logtostderr=1 2>&1 | tee $3/benchmark.log
$1 time -model $2 -iterations=$4 -logtostderr=1 2>&1 | tee $3/benchmark.log
echo
echo "Benchmark beendet. Log-File befindet sich in " $3

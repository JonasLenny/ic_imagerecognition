package helpers;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

public abstract class ArrayFunctions 
{
	public static int[] createRandomList(int start, int end, int count)
	{
		Random random = new Random();
		Set<Integer> randomList = new LinkedHashSet<Integer>();
		
		int range = end - start;
		
		while(randomList.size() < count)
			randomList.add(random.nextInt(range) + start);
		
		int[] list = new int[randomList.size()];
		int listPointer = 0;
		
		Iterator iterator = randomList.iterator();
		
		while (iterator.hasNext())
		    list[listPointer++] = (int)iterator.next();
		
		return list;
	}
}

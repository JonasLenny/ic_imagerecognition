/**
 * 
 */
package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import helpers.ArrayFunctions;

/**
 * @author Jonas
 *
 */
public class DatasetHandler 
{
	private static String dataPath;
	private static String destinationPath;
	
	private static int imagesPerSet;
	private static String[] usedCategories;
	private static boolean isAbsolute;
	
	private static String[] labelMap;
	private static int categoryCount;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		validateArguments(args);
		createList(args);
	}
	
	private static void createList(String[] arguments)
	{
		System.out.println("Erstelle Liste mit den Bildern");
		
		String labelList = "";
		labelMap = new String[categoryCount];
		
		File[] categories = new File(dataPath).listFiles();
		int labelCount = 0;
		
		for(int i = 0; i < categories.length; ++i)
			if(isListed(categories[i].getName()))
			{
				labelMap[labelCount] = categories[i].getName();
				labelList += writeImagesToList(categories[i], labelCount++);
			}
		
		writeLabelList(labelList);
		writeInfo();
		writeLabelMap(labelMap);
		System.out.println("Alles fertig");
		
	}

	private static String writeImagesToList(File file, int labelNumber) 
	{
		System.out.println("  Lese Kategorie " + file.getName());
		String categoryList = "";
		File[] childrens = file.listFiles();
		
		int[] childrenIndeces = ArrayFunctions.createRandomList(0, childrens.length, imagesPerSet);
		
		for(int i = 0; i < childrenIndeces.length; ++i)
		{
			int fileIndex = childrenIndeces[i];
			File currentChildren = childrens[fileIndex];
			String label = currentChildren.getName();
			String filePath = "";
			
			if(label.endsWith(".png"))
			{
				try {
					filePath = isAbsolute ? currentChildren.getCanonicalPath() : file.getName() + "/" + currentChildren.getName();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				categoryList += filePath + " " + labelNumber + System.lineSeparator();
			}
		}
		
		return categoryList;
	}

	private static boolean isListed(String cat) 
	{
		boolean isListed = false;
		
		for(int i = 0; i < usedCategories.length; ++i)
			if(usedCategories[i].equals(cat))
			{
				isListed = true;
				break;
			}
		
		return isListed;
	}

	private static void validateArguments(String[] args) 
	{
		System.out.println("�berpr�fe die Parameter");
		
		// erstes Argument muss ein g�ltiger Pfad zum Ordner der Bilder sein
		dataPath = checkPath(args[0]);
		
		// zweite Argument muss ein g�ltiger Pfad zum Ablageort der Dateien sein.
		destinationPath = checkPath(args[1]);
		
		// ist die Anzahl der Bilder pro Set
		imagesPerSet = checkNumber(args[2]);
		
		// welche Kategorien sollen verwendet werden
		usedCategories = checkCategories(args[3], dataPath);
		
		// soll der absolute Pfad in die Liste oder der relative
		isAbsolute = checkBoolean(args[4]);
		
		System.out.println("Parameter sind in Ordnung");
	}

	private static boolean checkBoolean(String booleanValue) 
	{
		if(!(booleanValue.equals("true") || booleanValue.equals("false")))
			throw new RuntimeException("kein g�ltiger Boolean Wert");
		
		return Boolean.valueOf(booleanValue);
	}

	private static String checkPath(String path)
	{
		String valid_path = "";
		
		if(new File(path).exists())
			valid_path = path;
		
		if(!valid_path.endsWith("/"))
			valid_path += "/";
		
		return valid_path;
	}
	
	private static int checkNumber(String number)
	{
		int int_number = -1;
		
		try
	      {
	         int_number = Integer.parseInt(number);
	      }
	      catch (NumberFormatException ex)
	      {
	         System.out.println(ex);
	      }
		
		return int_number;
	}

	private static String[] checkCategories(String cats, String path) 
	{
		String[] categories = null;
		
		if(cats.equals("all"))
		{
			File[] childrens = new File(path).listFiles();
			categories = new String[childrens.length];
			
			for(int i = 0; i < childrens.length; ++i)
				categories[i] = childrens[i].getName();
		}
		else
			categories = cats.split("/");
		
		categoryCount = categories.length;
		
		return categories;
	}
	
	private static void writeLabelMap(String[] labelMap) 
	{
		String text = "";
		
		for(int i = 0; i < labelMap.length; ++i)
			text += i + " " + labelMap[i] + System.lineSeparator();
		
		File map = new File(destinationPath + "label_map.txt");
		
		writeFile(text, map);
	}

	private static void writeInfo() 
	{
		String text = "";
		text += "##################" + System.lineSeparator();
		text += "# Infos zum Set: #" + System.lineSeparator();
		text += "##################" + System.lineSeparator();
		text += System.lineSeparator();
		text += "Ursprungsset: \t\t\t" + dataPath + System.lineSeparator();
		text += "Bilder pro Kategorie: \t" + imagesPerSet + System.lineSeparator();
		
		String categories = "";
		
		for(int i = 0; i < usedCategories.length; ++i)
			categories += i != usedCategories.length-1 ? usedCategories[i] + ", " : usedCategories[i];
		
		text += "Verwendete Kategorien: \t" + categories + System.lineSeparator();
		
		File info = new File(destinationPath + "info.txt");
		writeFile(text, info);
	}

	private static void writeLabelList(String labelList) 
	{
		File list = new File(destinationPath + "labellist.txt");
		writeFile(labelList, list);
	}
	
	private static void writeFile(String text, File file)
	{
		System.out.println("Schreibe Datei " + file.getName());
		
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(file)))
		{
			writer.write(text);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

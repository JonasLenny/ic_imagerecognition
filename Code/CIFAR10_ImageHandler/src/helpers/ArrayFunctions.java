package helpers;

public class ArrayFunctions 
{
	public static int[] convertByteToInt(byte[] array)
	{
		int[] convertedArray = new int[array.length];
		
		for(int i = 0; i < array.length; ++i)
			convertedArray[i] = (int)array[i];
		
		return convertedArray;
	}
}

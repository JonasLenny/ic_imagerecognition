package main;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Locale.Category;

import javax.imageio.ImageIO;

import helpers.ArrayFunctions;
import javafx.scene.image.Image;

public class Reader 
{
	private final static String[] CLASSNAMES  = {"airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"};
	private final static String[] TRAIN_FILES = {"data_batch_1.bin", "data_batch_2.bin", "data_batch_3.bin", "data_batch_4.bin", "data_batch_5.bin"};
	private final static String[] TEST_FILES  = {"test_batch.bin"};
			
	private final static String OUTPUT_PATH = "cifar10_images/";
	private final static String FORMAT = "png";
	
	private final static int BINARY_IMAGE_LENGTH = 3073;
	private final static int NUMBER_OF_CHANNELS = 3;
	private final static int WIDTH = 32;
	private final static int HEIGHT = 32;
	
	private static int[] categoryCount;
	
	public static void main(String[] args) 
	{	
		create_ImagesFromBinary(TRAIN_FILES, "training");
		create_ImagesFromBinary(TEST_FILES, "test");
	}
	
	private static void create_ImagesFromBinary(String[] files, String subDirectory)
	{
		String outputDirPath = OUTPUT_PATH + subDirectory;
		categoryCount = new int[CLASSNAMES.length];
		
		if(checkDir(outputDirPath))
			for(int i = 0; i < files.length; ++i)
				create_Images(files[i], outputDirPath);
	}
	
	private static void create_Images(String file, String outputDir)
	{
		System.out.println("Lese " + file);
		
		try 
		{
			File binFile = new File(file);
		    FileInputStream inputStream = new FileInputStream(binFile);
		    int[] fileData = new int[(int) binFile.length()];

		    int c;
		    int count = 0;
		    while ((c = inputStream.read()) != -1) {
		    	fileData[count++] = c;
		    }
		    
		    int numberOfImages = fileData.length / BINARY_IMAGE_LENGTH;
		    for(int i = 0; i < numberOfImages; ++i)
		    {
		    	System.out.println("Lese Bild: " + i);
		    	int pos = i * BINARY_IMAGE_LENGTH;
		    	int end = pos + BINARY_IMAGE_LENGTH;
		    	
		    	int labelNumber = fileData[pos];
		    	
		    	
		    	String label = CLASSNAMES[fileData[pos]];
		    	String categoryDir = outputDir + "/" + CLASSNAMES[fileData[pos]];
		    	
		    	if(checkDir(categoryDir))
		    	{
		    		int[] imageBytes = Arrays.copyOfRange(fileData, pos + 1, end);
		    		BufferedImage image = create_Image(imageBytes, false);
		    		
		    		String fileName = label + "_" + categoryCount[labelNumber] + "." + FORMAT;
		    		String filePath = categoryDir + "/" + fileName;
		    		
		    		save_Image(image, filePath);
		    		++categoryCount[labelNumber];
		    	}
		    }
		    
		    inputStream.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	private static boolean checkDir(String directory)
	{
		boolean mkdir_succeeded = false;
		
		File outputDir = new File(directory);
		
		if(!outputDir.exists())
		{
			System.out.println(directory + " wird erstellt.");
			
			try 
			{
				outputDir.mkdir();
				mkdir_succeeded = true;
			} 
			catch(SecurityException e) 
			{
				System.out.println(e);
			}
		}
		else
			mkdir_succeeded = true;
		
		return mkdir_succeeded;
	}
	
	private static BufferedImage create_Image(int[] binImage, boolean inOrder)
	{
		int channelLength  = binImage.length / NUMBER_OF_CHANNELS;
		int[] rgb_intArray = new int[channelLength];
		
		Arrays.fill(rgb_intArray, 0xFF000000);
		
		for(int i = 0; i < channelLength; ++i)
		{
			int rValue = binImage[i];
			int gValue = binImage[channelLength * 1 + i];
			int bValue = binImage[channelLength * 2 + i];
			
			rgb_intArray[i] = (0xFF << 24) | (rValue << 16) | (gValue << 8) | bValue;
		}
		
		BufferedImage tmpImage = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		tmpImage.setRGB(0, 0, WIDTH, HEIGHT, rgb_intArray, 0, WIDTH);

		return tmpImage;
	}
	
	private static void save_Image(BufferedImage image, String filePath)
	{
		try
		{
			File imageFile = new File(filePath);
			ImageIO.write(image, FORMAT, imageFile);
		}
		catch(IOException e)
		{
			System.out.println(e);
		}
	}

}

package sites.subwindows;

import controller.LayerWindowController;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import models.Layer;
import models.Model;

public class LayerWindow 
{
	private int width = 400;
	private int height = 600;

	private Stage subwindow;
	private Layer layer;
	private Model model;
	private LayerWindowController windowController;
	
	private TextArea textArea;
	
	public LayerWindow(Model model, Layer layer)
	{
		this.layer = layer;
		this.model = model;
		windowController = new LayerWindowController(model, layer, this);
		
		subwindow = new Stage();
		subwindow.setResizable(false);
		
		VBox  content	= createContent(layer);
		Scene scene 	= new Scene(content, width, height);
		
//		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		
		subwindow.setOnCloseRequest(windowController::onWindowClose);
		subwindow.setTitle(layer.getId());
		subwindow.setScene(scene);
		subwindow.show();
	}

	private VBox createContent(Layer layer) 
	{
		VBox content = new VBox();
		VBox.setVgrow(content, Priority.ALWAYS);
		content.setSpacing(5);
		content.setPadding(new Insets(5.0));
//		content.setStyle("-fx-border-style: solid;"
//	  			  + "-fx-border-width: 1;"
//	  			  + "-fx-border-color: green;");
		addButtonBar(content);
		addTextField(content);
		return content;
	}
	
	private void addButtonBar(VBox parent)
	{
		Button button0 = new Button("Speichern");
		button0.addEventHandler(ActionEvent.ACTION, windowController::onButtonSave);
		
		Button button1 = new Button("Verwerfen");
		button1.addEventHandler(ActionEvent.ACTION, windowController::onButtonClose);
		
		HBox buttonBar = new HBox();
		buttonBar.setSpacing(40);
		buttonBar.setAlignment(Pos.CENTER);
		buttonBar.getChildren().addAll(button0, button1);
		
		parent.getChildren().add(buttonBar);		
	}
	
	private void addTextField(VBox content) 
	{
		textArea = new TextArea(layer.getData());
		textArea.prefWidth(width);
		textArea.setPrefHeight(height);
		textArea.setWrapText(false);
		
		content.getChildren().add(textArea);
	}

	public Stage getSubwindow() {
		return subwindow;
	}
	
	public TextArea getTextArea() {
		return textArea;
	}
}

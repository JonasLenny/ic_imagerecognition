package sites;

import handler.DataHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import models.EntryButton;
import models.Model;

public class Entry extends GridPane
{
	private int spacing = 50;
	private int spacing2 = 90;
	private double fontSize = 15;
	
	private TabPane tabpane;
	private Model model;
	private DataHandler dataHandler;
	
	public Entry(Model model, TabPane tabpane)
	{
		this.model = model;
		this.dataHandler = this.model.getDataHandler();
		
		this.tabpane = tabpane;
		this.setAlignment(Pos.CENTER);
		this.setVgap(40);
		this.setHgap(40);
		this.setPadding(new Insets(0, 25, 25, 25));
		
		addButtons();
	}
	
	private void addButtons()
	{
		HBox firstLineBox = new HBox();
		firstLineBox.setSpacing(spacing);
		
		HBox secondLineBox = new HBox();
		secondLineBox.setPadding(new Insets(0,0,0,70));
		secondLineBox.setSpacing(spacing2);
		
		Image button0Icon 	= dataHandler.getImage("0");
		EntryButton button0 = new EntryButton("0", "Netzwerk verwenden", button0Icon, fontSize, this::buttonHandler);
		
		Image button1Icon 	= dataHandler.getImage("1");
		EntryButton button1 = new EntryButton("1", "Netzwerk bauen", button1Icon, fontSize, this::buttonHandler);
		
		Image button2Icon 	= dataHandler.getImage("2");
		EntryButton button2 = new EntryButton("2", "Auswertung", button2Icon, fontSize, this::buttonHandler);
		
		Image button3Icon 	= dataHandler.getImage("3");
		EntryButton button3 = new EntryButton("3", "Einstellungen", button3Icon, fontSize, this::buttonHandler);
		
		Image button4Icon 	= dataHandler.getImage("4");
		EntryButton button4 = new EntryButton("4", "Tutorial", button4Icon, fontSize, this::buttonHandler);
		
		firstLineBox.getChildren().addAll(button0, button1, button2);
		secondLineBox.getChildren().addAll(button3, button4);
		
		this.add(firstLineBox, 0, 0);
		this.add(secondLineBox, 0, 1);
	}
	
	public void buttonHandler(ActionEvent event)
	{
		Button src = (Button) event.getSource();
		String id = src.getId();
		System.out.println("id: " + id);
		
		SingleSelectionModel<Tab> selectionModel = tabpane.getSelectionModel();
		selectionModel.select(Integer.valueOf(id));
		
		Scene tabScene = new Scene(tabpane, model.getWindowWidth(), model.getWindowHeight());
		model.getMainStage().setScene(tabScene);
	}
}

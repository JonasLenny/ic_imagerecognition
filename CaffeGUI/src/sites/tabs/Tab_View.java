package sites.tabs;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import models.Model;

public class Tab_View extends TabPane
{
	public Tab_View(Model model)
	{
		Tab tabUse 		= new Tab_Use();
		Tab tabCreate 	= new Tab_Create(model);
		Tab tabAnalysis = new Tab_Analysis();
		Tab tabSettings = new Tab_Settings();
		Tab tabInformations = new Tab_Informations();
		
		this.getTabs().add(tabUse);
		this.getTabs().add(tabCreate);
		this.getTabs().add(tabAnalysis);
		this.getTabs().add(tabSettings);
		this.getTabs().add(tabInformations);
	}
}

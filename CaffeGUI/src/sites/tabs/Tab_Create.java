package sites.tabs;


import javax.swing.GroupLayout.Alignment;

import controller.CanvasController;
import controller.LayerController;
import controller.OptionButtonController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Tab;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Pair;
import models.Layer;
import models.Model;
import models.OptionbarButton;

public class Tab_Create extends Tab
{
	private Model model;
	private HBox rootBox;
	private HBox optionBar;
	private Pane canvas;
	
	public Tab_Create(Model model)
	{
		this.setText(" Netzwerk erstellen ");
		this.setClosable(false);
		this.model = model;
		
		rootBox = new HBox();
		rootBox.setId("rootBox");
		
		rootBox.getChildren().add(createLayers());
		rootBox.getChildren().add(createContentArea());
		
		this.setContent(rootBox);
	}
	
	/**
	 * Erzeugt das linke Menue in dem sich alle verfuegbaren Schichten fuer das
	 * Netzwerk befinden.
	 * 
	 * @return scrollPane Eine ScrollPane mit den Schichten.
	 */
	private ScrollPane createLayers()
	{
		// rootPane des Menues
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		scrollPane.setMinWidth(200.0);
		
		VBox layers = new VBox();
		layers.setId("layers");
		layers.setSpacing(5);
		layers.setMinWidth(scrollPane.getMinWidth());
		
		createContent(layers);
		scrollPane.setContent(layers);
		return scrollPane;
	}	

	private void createContent(VBox layers) 
	{
		Pair<String, String[]>[] allTypes = model.getDataHandler().getAllLayer();
		
		for(int i = 0; i < allTypes.length; ++i)
		{
			// Der Kopf fuer jeden Typ
			// Name
			String rawName = allTypes[i].getKey();
			String firstLetter = rawName.substring(0, 1).toUpperCase();
			String formattedName = firstLetter + rawName.substring(1);
			
			Label layerType = new Label(formattedName);
			layerType.setFont(Font.font(null, FontWeight.BOLD, 20));
			
			HBox categoryHead = new HBox();
			categoryHead.setPadding(new Insets(0, 0, 0, 5));
			categoryHead.setStyle("-fx-background-color: darkgray");
			categoryHead.getChildren().add(layerType);
			
			layers.getChildren().add(categoryHead);
			
			// die einzelnen verfuegbaren Schichten pro Typ
			VBox typeBox = new VBox();
			typeBox.setSpacing(5);
			typeBox.setPadding(new Insets(0, 5, 5, 5));
			
			String[] typeLayers = allTypes[i].getValue();
			LayerController layerController = new LayerController(model);
			
			for(int j = 0; j < typeLayers.length; ++j)
			{
				Layer newLayer = new Layer(model, rawName, typeLayers[j], layerController, true);
				typeBox.getChildren().add(newLayer);
			}
			
			layers.getChildren().add(typeBox);
		}
	}
	
	private VBox createContentArea()
	{
		VBox contentArea = new VBox();
		HBox.setHgrow(contentArea, Priority.ALWAYS);
		contentArea.setStyle("-fx-border-style: solid;"
	              		   + "-fx-border-width: 1;"
	              		   + "-fx-border-color: black");
		createOptionBar(contentArea);
		createBuildArea(contentArea);
		return contentArea;
	}
	
	private void createOptionBar(VBox parent)
	{
		// Optionsleiste fuer die Buttons
		optionBar = new HBox();
		optionBar.setId("optionBar");
		optionBar.setAlignment(Pos.BASELINE_RIGHT);
		optionBar.setSpacing(10.0);
		optionBar.setPadding(new Insets(5.0));
		
		OptionButtonController optionController = new OptionButtonController(model);		
		// Buttons mit den Optionen
		OptionbarButton button0 = new OptionbarButton("Speichern", optionController::onDeleteAll);
		OptionbarButton button1 = new OptionbarButton("Laden", optionController::onDeleteAll);
		OptionbarButton button2 = new OptionbarButton("Verwenden", optionController::onDeleteAll);
		OptionbarButton button3 = new OptionbarButton("Alles l�schen", optionController::onDeleteAll);
		
		// hinzufuegen der Button zur Optionsleiste
		optionBar.getChildren().addAll(button0, button1, button2, button3);

		// hinzufuegen der Leiste zum darueberliegenden Node
		parent.getChildren().add(optionBar);
	}
	
	private void createBuildArea(VBox parent)
	{
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.prefViewportHeightProperty().bind(parent.heightProperty());
		scrollPane.prefViewportWidthProperty().bind(parent.widthProperty());
		
		canvas = new Pane();
		canvas.setId("canvas");
		canvas.prefHeightProperty().bind(scrollPane.heightProperty());
		canvas.prefWidthProperty().bind(scrollPane.widthProperty());
		
		CanvasController canvasController = new CanvasController(model, canvas);
		canvas.setOnDragOver(canvasController::onDragOver);
		canvas.setOnDragEntered(canvasController::onDragEntered);
		canvas.setOnDragDropped(canvasController::onDragDropped);
		
		scrollPane.setContent(canvas);
		parent.getChildren().add(scrollPane);
		
		model.setCanvas(canvas);
	}
}

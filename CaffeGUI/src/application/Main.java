package application;
	
import handler.DataHandler;
import javafx.application.Application;
import javafx.stage.Stage;
import models.Model;
import sites.Entry;
import sites.tabs.Tab_View;
import javafx.scene.Scene;
import javafx.scene.control.Tab;

public class Main extends Application
{
	@Override
	public void start(Stage primaryStage)
	{
		try
		{
			DataHandler dataHandler = new DataHandler();
			Model model = new Model();
			model.setMainStage(primaryStage);
			model.setDataHandler(dataHandler);
			
			Tab_View tabview = new Tab_View(model);
			Entry entry = new Entry(model, tabview);
			
			Scene scene = new Scene(entry, model.getWindowWidth(), model.getWindowHeight());
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			primaryStage.setScene(scene);
			primaryStage.setTitle("Lenny's masterpiece");
			primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}
}

package handler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javafx.util.Pair;

public class LayerHandler 
{
	private ArrayList<Pair<String, String[]>>layers;
	private int layersCount;
	
	public LayerHandler(String path)
	{
		layers = new ArrayList<>();
		layersCount = 0;
		
		recursiveLayersSearch(path, layers);
		System.out.println("layers load complete, number of layers: " + layersCount);
	}

	private void recursiveLayersSearch(String path, ArrayList<Pair<String, String[]>> layers)
	{
		File[] files = new File(path).listFiles();
		
		String dirName = path.substring(path.lastIndexOf(File.separator) + 1);
		ArrayList<String> typeLayers = new ArrayList<String>();;
		
		for(int i = 0; i < files.length; ++i)
		{
			if(files[i].isDirectory())
			{
				String modifiedBasePath = path + File.separator + files[i].getName(); 
				recursiveLayersSearch(modifiedBasePath, layers);
			}
			else
			{
				File actFile = files[i];
				String fileName = actFile.getName();
				String lowerCaseName = fileName.toLowerCase();
				
				if(lowerCaseName.endsWith(".txt"))
				{
					try {
						typeLayers.add(actFile.getCanonicalPath());
						++layersCount;
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		if(!typeLayers.isEmpty())
		{
			String typeName = dirName.split("_")[1];
			String[] layersAsArray = typeLayers.toArray(new String[typeLayers.size()]);
			Pair<String, String[]> newLayerType = new Pair<String, String[]>(typeName, layersAsArray);
			layers.add(newLayerType);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Pair<String, String[]>[] getLayers() 
	{
		return layers.toArray(new Pair[layers.size()]);
	}
}

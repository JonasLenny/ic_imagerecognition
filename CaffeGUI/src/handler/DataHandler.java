package handler;

import java.io.File;
import java.util.ArrayList;

import javafx.scene.image.Image;
import javafx.util.Pair;

public class DataHandler 
{
	private final String IMAGES = "images" + File.separator;
	private final String LAYERS = "layers" + File.separator;
	
	private ImageHandler imageHandler;
	private LayerHandler layerHandler;
	
	private Pair<String, Image>[] images;
	private Pair<String, String[]>[] layers;
	
	public DataHandler()
	{
		imageHandler = new ImageHandler(IMAGES);
		layerHandler = new LayerHandler(LAYERS);
		
		images = imageHandler.getImages();
		layers = layerHandler.getLayers();
	}
		
	public Image getImage(String name)
	{
		Image searchedImage = null;
		
		for(int i = 0; i < images.length; ++i)
			if(images[i].getKey().equals(name))
			{
				searchedImage = images[i].getValue();
				break;
			}

		return searchedImage;
	}
	
	public Pair<String, String[]>[] getAllLayer()
	{
		return layers;
	}
}

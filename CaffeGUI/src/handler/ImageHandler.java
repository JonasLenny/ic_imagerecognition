package handler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javafx.scene.image.Image;
import javafx.util.Pair;

public class ImageHandler 
{
	private ArrayList<Pair<String, Image>> images;
	private int imageCount;
	
	public ImageHandler(String path)
	{
		images = new ArrayList<>();
		imageCount = 0;
		
		recursiveImageSearch(path, images);
		System.out.println("image load complete, number of images: " + imageCount);
	}
	
	private void recursiveImageSearch(String path, ArrayList<Pair<String, Image>> tmpImages)
	{
		File[] files = new File(path).listFiles();
		
		for(int i = 0; i < files.length; ++i)
		{
			if(files[i].isDirectory())
			{
				String modifiedBasePath = path + File.separator + files[i].getName(); 
				recursiveImageSearch(modifiedBasePath, tmpImages);
			}
			else
			{
				File actImg = files[i];
				String fullImgName = actImg.getName();
				String lowerCaseName = fullImgName.toLowerCase();
				
				if(lowerCaseName.endsWith(".png") || lowerCaseName.endsWith(".jpg"))
				{
					String imgName = fullImgName.substring(0, fullImgName.lastIndexOf("."));
					
					Image image = new Image("file:" + actImg.getPath());
					tmpImages.add(new Pair<String, Image>(imgName, image));
					++imageCount;
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public Pair<String, Image>[] getImages() 
	{
		return images.toArray(new Pair[images.size()]);
	}
}

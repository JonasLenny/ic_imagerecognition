package models;

import java.util.ArrayList;

import handler.DataHandler;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Model 
{
	private int windowWidth = 1200;
	private int windowHeight = 800;
	
	private Stage mainStage;
	private DataHandler dataHandler;
	
	private Layer draggedLayer;
	private Pane canvas;
	private ArrayList<Layer> activeLayerWindows;
	
	public Model()
	{
		activeLayerWindows = new ArrayList<>();
	}
	
	public boolean isActiveLayerWindow(Layer layer) 
	{
		boolean isActive = true;
		
		if(!activeLayerWindows.contains(layer))
			isActive = false;
		
		return isActive;
	}
	
	public void addActiveLayerWindow(Layer layer) 
	{
		activeLayerWindows.add(layer);
	}
	
	public void removeActiveLayer(Layer layer)
	{
		if(activeLayerWindows.contains(layer))
			activeLayerWindows.remove(layer);
	}
	
	public int getWindowWidth() {
		return windowWidth;
	}
	public void setWindowWidth(int windowWidth) {
		this.windowWidth = windowWidth;
	}
	public int getWindowHeight() {
		return windowHeight;
	}
	public void setWindowHeight(int windowHeight) {
		this.windowHeight = windowHeight;
	}
	public Stage getMainStage() {
		return mainStage;
	}
	public void setMainStage(Stage mainStage) {
		this.mainStage = mainStage;
	}
	public DataHandler getDataHandler() {
		return dataHandler;
	}
	public void setDataHandler(DataHandler dataHandler) {
		this.dataHandler = dataHandler;
	}
	public Layer getDraggedLayer() {
		return draggedLayer;
	}
	public void setDraggedLayer(Layer draggedLayer) {
		this.draggedLayer = draggedLayer;
	}
	public Pane getCanvas() {
		return canvas;
	}
	public void setCanvas(Pane canvas) {
		this.canvas = canvas;
	}
}

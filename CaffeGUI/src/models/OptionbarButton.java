package models;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

public class OptionbarButton extends Button
{
	public OptionbarButton(String label, EventHandler<ActionEvent> handler)
	{
		super(label);
		this.setMinHeight(30.0);
		this.setMinWidth(80.0);
		this.addEventHandler(ActionEvent.ACTION, handler);
	}
}

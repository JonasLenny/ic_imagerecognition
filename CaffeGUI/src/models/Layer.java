package models;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import controller.LayerController;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Layer extends HBox
{
	private Model model;
	private LayerController layerController;
	
	private VBox secondView;
	
	private String layerName = "default"; 
	private String dataPath;
	private String data;
	
	private Image icon;
	private String iconPath;
	
	private Image drag_icon;
	
	public Layer(Model model, String iconPath, String dataPath, LayerController layerController, boolean loadData)
	{
		this.model = model;
		this.data = "";
		this.iconPath = iconPath;
		this.dataPath = dataPath;
		this.layerController = layerController;
		
		createFirstView(iconPath, dataPath);
		createSecondView();
		loadData(loadData, dataPath);
		
		this.setId(layerName);
		this.setAlignment(Pos.CENTER_LEFT);
		this.setSpacing(25.0);
		this.setOnDragDetected(layerController::onCopyDrag);
//		this.setStyle("-fx-border-style: solid;"
//                	+ "-fx-border-width: 1;"
//                	+ "-fx-border-color: black");
	}
	
	private void createFirstView(String iconPath, String dataPath)
	{
		icon 	  = model.getDataHandler().getImage(iconPath);
		drag_icon = model.getDataHandler().getImage(iconPath + "_drag");
		
		String fullName 		 = dataPath.substring(dataPath.lastIndexOf(File.separator) + 1);
		String nameWithoutEnding = fullName.substring(0, fullName.lastIndexOf("."));
		String firstLetter 		 = nameWithoutEnding.substring(0, 1).toUpperCase();
		
		layerName  = firstLetter + nameWithoutEnding.substring(1);
		Label text = new Label(layerName);
		text.setFont(Font.font(null, FontWeight.BOLD, 15));
		
		ImageView imageView = new ImageView(icon);
		imageView.setFitWidth(40);
		imageView.setPreserveRatio(true);
		
		this.getChildren().addAll(imageView, text);
	}
	
	private void createSecondView()
	{
		ImageView iconView = new ImageView(icon);
		iconView.setFitWidth(40);
		iconView.setPreserveRatio(true);
		
		Label lName = new Label(layerName);
		lName.setFont(Font.font(null, FontWeight.BOLD, 15));
		
		secondView = new VBox();
		secondView.setAlignment(Pos.CENTER);
		secondView.getChildren().addAll(iconView, lName);
	}
	
	private void loadData(boolean loadData, String dataPath)
	{
		if(loadData)
		{
			try (BufferedReader bufferedReader = new BufferedReader(new FileReader(dataPath)))
			{
				String sCurrentLine = "";
				while ((sCurrentLine = bufferedReader.readLine()) != null) 
					data += sCurrentLine + System.lineSeparator();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void changeToSecondMode()
	{
		this.getChildren().clear();
		this.getChildren().add(secondView);
		this.setOnDragDetected(layerController::onMoveDrag);
		this.setOnMouseClicked(layerController::onDoubleClick);
	}
	
	public Layer getCopy()
	{
		Layer copyOfMe = new Layer(model, iconPath, dataPath, layerController, false);
		copyOfMe.setData(data);
		
		return copyOfMe;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Image getIcon() {
		return icon;
	}

	public Image getDrag_icon() {
		return drag_icon;
	}
}

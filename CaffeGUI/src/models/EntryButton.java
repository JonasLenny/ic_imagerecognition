package models;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class EntryButton extends VBox
{
	private int innerSpacing = 5;
	
	public EntryButton(String id, String labelText, Image icon, double fontSize, EventHandler<ActionEvent> function)
	{
		ImageView buttonIcon = new ImageView(icon);
		buttonIcon.setPreserveRatio(true);
		buttonIcon.setFitWidth(110);
		
		Button button = new Button();
		button.setGraphic(buttonIcon);
		button.setId(id);
		button.addEventHandler(ActionEvent.ACTION, function);
		
		Label label = new Label(labelText);
		label.setFont(new Font(fontSize));
		
		this.setAlignment(Pos.CENTER);
		this.setSpacing(innerSpacing);
		this.getChildren().addAll(button, label);
	}
}

package controller;

import javafx.event.ActionEvent;
import models.Model;

public class OptionButtonController 
{
	private Model model;
	
	public OptionButtonController(Model model) 
	{
		this.model = model;
	}
	
	public void onDeleteAll(ActionEvent event)
	{
		model.getCanvas().getChildren().clear();
		event.consume();
	}
}

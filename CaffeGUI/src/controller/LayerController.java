package controller;

import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import models.Layer;
import models.Model;
import sites.subwindows.LayerWindow;

public class LayerController 
{
	private Model model;
	
	public LayerController(Model model) 
	{
		this.model = model;
	}

	public void onCopyDrag(MouseEvent mouseEvent)
	{
		Layer source = (Layer) mouseEvent.getSource();
		model.setDraggedLayer(source);
		System.out.println("on drag detected by " + source.getId() + " as copy");
		
		Dragboard dragboard = source.startDragAndDrop(TransferMode.COPY);
		ClipboardContent content = new ClipboardContent();
		content.putImage(source.getDrag_icon());
		dragboard.setContent(content);
		
		mouseEvent.consume();
	}
	
	public void onMoveDrag(MouseEvent mouseEvent)
	{
		Layer source = (Layer) mouseEvent.getSource();
		model.setDraggedLayer(source);
		System.out.println("on drag detected by " + source.getId() + " as move");
		
		Dragboard dragboard = source.startDragAndDrop(TransferMode.MOVE);
		ClipboardContent content = new ClipboardContent();
		content.putImage(source.getDrag_icon());
		dragboard.setContent(content);
		
		mouseEvent.consume();
	}
	
	public void onDoubleClick(MouseEvent mouseEvent)
	{
		Layer source = (Layer) mouseEvent.getSource();
		
		if(mouseEvent.getClickCount() == 2)
		{
			if(!model.isActiveLayerWindow(source))
			{
				model.addActiveLayerWindow(source);
				LayerWindow layerContent = new LayerWindow(model, source);
			}
			else
			{
				System.out.println("already open");
			}
		}
	}
}

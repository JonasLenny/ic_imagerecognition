package controller;

import javafx.event.ActionEvent;
import javafx.stage.WindowEvent;
import models.Layer;
import models.Model;
import sites.subwindows.LayerWindow;

public class LayerWindowController 
{
	private LayerWindow layerWindow;
	private Model model;
	private Layer layer;
	
	public LayerWindowController(Model model, Layer layer, LayerWindow layerWindow)
	{
		this.layerWindow = layerWindow;
		this.model = model;
		this.layer = layer;
	}
	
	public void onWindowClose(WindowEvent windowEvent)
	{
		model.removeActiveLayer(layer);
	}
	
	public void onButtonClose(ActionEvent event)
	{
		model.removeActiveLayer(layer);
		layerWindow.getSubwindow().close();
	}
	
	public void onButtonSave(ActionEvent event)
	{
		String text = layerWindow.getTextArea().getText();
		layer.setData(text);
		
		model.removeActiveLayer(layer);
		layerWindow.getSubwindow().close();
	}
}

package controller;

import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import models.Layer;
import models.Model;

public class CanvasController 
{
	private Pane canvas;
	private Model model;
	
	public CanvasController(Model model, Pane canvas)
	{
		this.model = model;
		this.canvas = canvas;
	}
	
	public void onDragOver(DragEvent dragEvent)
	{
//		System.out.println("onDragOver");
		
		if(dragEvent.getGestureSource() != canvas)
			dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		
		dragEvent.consume();
	}
	
	public void onDragEntered(DragEvent dragEvent)
	{
		System.out.println("onDragEntered");
		
		if (dragEvent.getGestureSource() != canvas)
		{
//        	t.setFill(Color.GREEN);
        }
		
		dragEvent.consume();
	}
	
	public void onDragDropped(DragEvent dragEvent)
	{
		Dragboard dragboard = dragEvent.getDragboard();
		boolean success = false;
		
		Layer draggedLayer = model.getDraggedLayer();
		
		if(dragEvent.getTransferMode() == TransferMode.COPY)
		{
			success = true;
			Layer newLayer = draggedLayer.getCopy();
			newLayer.changeToSecondMode();
			
			double centeredPosX = dragEvent.getX() - (newLayer.getWidth() / 2);
			double centeredPosY = dragEvent.getY() - (newLayer.getHeight() / 2);
			
			newLayer.setLayoutX(centeredPosX);
			newLayer.setLayoutY(centeredPosY);
			
			canvas.getChildren().add(newLayer);
		}
		else if(dragEvent.getTransferMode() == TransferMode.MOVE)
		{
			success = true;
			
			double centeredPosX = dragEvent.getX() - (draggedLayer.getWidth() / 2);
			double centeredPosY = dragEvent.getY() - (draggedLayer.getHeight() / 2);
			
			draggedLayer.setLayoutX(centeredPosX);
			draggedLayer.setLayoutY(centeredPosY);
		}
		
		model.setDraggedLayer(null);
		
		dragEvent.setDropCompleted(success);
		dragEvent.consume();
	}
}
